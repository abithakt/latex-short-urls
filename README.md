# latex-short-urls

Shortens all URLs in a LaTeX file. Mostly adapted from [this answer on Stack 
Overflow](https://stackoverflow.com/questions/7676255/find-and-replace-urls-in-a-block-of-text-return-text-list-of-urls/7676609#7676609).

## Getting started

1. Download and install Python 3.
2. Install `pyshorteners` from pip.
3. Clone/download this repository.

Skip to Usage for usage instructions.

## Usage

```
python3 latex-short-urls.py path/to/latexfile
```

where `latexfile` is the file in which you want to shorten all URLs.

## Notes on this implementation

By default, 

* URLs are shortened with TinyURL. 
* Only URLs inside `\url{...}` are shortened.
