#!/usr/bin/python3
# adapted from https://stackoverflow.com/questions/7676255/find-and-replace-urls-in-a-block-of-text-return-text-list-of-urls

from sys import argv
import re
from pyshorteners import Shortener

shortener = Shortener('Tinyurl')
url_regex = re.compile(r"\\url{" + r"""(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>\[\]]+|\(([^\s()<>\[\]]+|(\([^\s()<>\[\]]+\)))*\))+(?:\(([^\s()<>\[\]]+|(\([^\s()<>\[\]]+\)))*\)|[^\s`!(){};:'".,<>?\[\]]))""" + "}")

def process_match(m):
    matches.append(m.group(0))
    #return 'URL'
    return r"\url{" + shortener.short(m.group(0)[5:-1]) + "}"
    #return m.group(0)

with open(argv[1], "r") as latex_file:
    text = open(argv[1], "r").read()
    matches = []

    new_text = url_regex.sub(process_match, text)

    #print(new_text)
    #print(matches)

with open(argv[1], "w") as latex_file:
    latex_file.write(new_text)
